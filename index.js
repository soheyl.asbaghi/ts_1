// TODO: 1.The Basics -->
// __Type Inferences__
var firstName = 'James'; //String
// firstName = 007; //!TypeError: Type 'number' is not assignable to type 'string'
console.log(firstName.toUpperCase());
console.log(firstName.length);
var arr = [1, 2, 3, 4];
// arr = ['1', '2', '3']; //!TypeError:Type 'string' is not assignable to type 'number'
var exist; //Any type
exist = true;
exist = 1;
// __Variable Type Annotations__
var phoneNumber;
if (Math.random() > 0.5) {
    phoneNumber = '+61770102062';
}
else {
    phoneNumber = '7167762323';
}
var superHero = ['Batman', 'SuperMan', 'WonderWoman'];
var number = [true, 'Two', 3];
var tuple = ['James', 'Bond', 7];
var lastName = 'Bond'; //Union Type
var bool = true; //Union Type
bool = 1;
var myFavNum = function () {
    return 6;
};
myFavNum();
function greet(name /*parameter type*/) {
    console.log("Hello, " + name + "!");
}
greet('Katz');
function log(message) {
    console.log(message);
}
log('Please login');
function triple(value) {
    return value * 3;
}
function greetTripled(greeting, value) {
    console.log(greeting + ", " + triple(value));
}
greetTripled('Hiya', 5);
//enum
var UserResponse;
(function (UserResponse) {
    UserResponse[UserResponse["No"] = 0] = "No";
    UserResponse[UserResponse["Yes"] = 1] = "Yes";
})(UserResponse || (UserResponse = {}));
var userExist = UserResponse.Yes;
// TODO: 2.Narrowing -->
// __Type guards__
function formatStatistic(stat) {
    if (typeof stat === 'number') {
        return stat.toFixed(2);
    }
    else if (typeof stat === 'string') {
        return stat.toUpperCase();
    }
    else {
        // do nothing
    }
}
// TODO: 3.More on Functions -->
// __Optional Parameters__
function proclaim(status) {
    console.log("I'm " + (status || 'not ready...'));
}
proclaim();
proclaim('ready?');
// __Default Parameters__
function agent(name) {
    if (name === void 0) { name = 'Anonymous'; }
    console.log("Hello, " + name + "!");
}
agent();
agent('James');
/**
 * Returns the sum of two numbers.
 *
 * @param x - The first input number
 * @param y - The second input number
 * @returns The sum of `x` and `y`
 *
 */
function getSum(x, y) {
    return x + y;
}
var person = function (name, lastName) {
    return "FullName: " + name + " " + lastName;
};
// __Rest Parameters__
var sum = function (x, y) {
    if (x === void 0) { x = 1; }
    var numbers = [];
    for (var _i = 2; _i < arguments.length; _i++) {
        numbers[_i - 2] = arguments[_i];
    }
    if (y)
        return x + y;
    return x;
};
console.log(sum(2));
console.log(sum(2, 2, 3, 4, 5, 6, 4));
// TODO: 3.object Type -->
var car = {
    make: 'Ford',
    model: 'Mustang',
    year: 1969,
    run: function (speed) {
        return "Car speed is " + speed;
    }
};
console.log(car);
var userOne = {
    userName: 'JamesMay',
    id: 121
};
