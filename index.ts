// TODO: 1.The Basics -->

// __Type Inferences__

let firstName = 'James'; //String
// firstName = 007; //!TypeError: Type 'number' is not assignable to type 'string'

console.log(firstName.toUpperCase());
console.log(firstName.length);

let arr = [1, 2, 3, 4];
// arr = ['1', '2', '3']; //!TypeError:Type 'string' is not assignable to type 'number'

let exist; //Any type
exist = true;
exist = 1;

// __Variable Type Annotations__

let phoneNumber: string;

if (Math.random() > 0.5) {
  phoneNumber = '+61770102062';
} else {
  phoneNumber = '7167762323';
}

let superHero: string[] = ['Batman', 'SuperMan', 'WonderWoman'];
let number: any[] = [true, 'Two', 3];
let tuple: [string, string, number] = ['James', 'Bond', 7];

let lastName: string | null = 'Bond'; //Union Type
let bool: boolean | number = true; //Union Type
bool = 1;

let myFavNum = function (): number /*Return type*/ {
  return 6;
};

myFavNum();

function greet(name: string /*parameter type*/) {
  console.log(`Hello, ${name}!`);
}

greet('Katz');

function log(message: string): void /*function without return*/ {
  console.log(message);
}

log('Please login');

function triple(value: number) {
  return value * 3;
}
function greetTripled(greeting: string, value: number) {
  console.log(`${greeting}, ${triple(value)}`!);
}

greetTripled('Hiya', 5);

//enum
enum UserResponse {
  No = 0,
  Yes = 1,
}

let userExist: UserResponse = UserResponse.Yes;

// TODO: 2.Narrowing -->
// __Type guards__

function formatStatistic(stat: string | number) {
  if (typeof stat === 'number') {
    return stat.toFixed(2);
  } else if (typeof stat === 'string') {
    return stat.toUpperCase();
  } else {
    // do nothing
  }
}

// TODO: 3.More on Functions -->

// __Optional Parameters__
function proclaim(status?: string) {
  console.log(`I'm ${status || 'not ready...'}`);
}

proclaim();
proclaim('ready?');

// __Default Parameters__
function agent(name = 'Anonymous') {
  console.log(`Hello, ${name}!`);
}
agent();
agent('James');

/**
 * Returns the sum of two numbers.
 *
 * @param x - The first input number
 * @param y - The second input number
 * @returns The sum of `x` and `y`
 *
 */
function getSum(x: number, y: number): number {
  return x + y;
}

let person = (name: string, lastName: string): string =>
  `FullName: ${name} ${lastName}`;

// __Rest Parameters__
let sum = (x: number = 1, y?: number, ...numbers: number[]) => {
  if (y) return x + y;
  return x;
};

console.log(sum(2));
console.log(sum(2, 2, 3, 4, 5, 6, 4));

// TODO: 3.object Type -->

let car: {
  make: string;
  model: string;
  year: number;
  run: (speed: number) => string;
} = {
  make: 'Ford',
  model: 'Mustang',
  year: 1969,
  run: (speed: number) => {
    return `Car speed is ${speed}`;
  },
};
console.log(car);

type User = {
  userName: string;
  id: number;
};

let user: User = {
  userName: 'JamesMay',
  id: 121,
};

user = {
  userName: 'RichardHammond',
  id: 141,
};
